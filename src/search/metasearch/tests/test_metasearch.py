import pytest
import tornado 
import tornado.testing
import json

from common.data_source import CSV
from geo.service import GeoService
from metasearch.service import MetaSearchService
from metasearch.http import get_service
from search.service import SearchInShardsService, SimpleSearchService
from settings import USER_DATA_FILE, GEO_DATA_FILE, SEARCH_DOCUMENTS_DATA_FILES
from user.service import UserService


@pytest.fixture
def metasearch():
    user_service = UserService(CSV(USER_DATA_FILE))
    geo_service = GeoService(CSV(GEO_DATA_FILE))
    search = SearchInShardsService(shards=[SimpleSearchService(CSV(file)) for file in SEARCH_DOCUMENTS_DATA_FILES])
    return MetaSearchService(search, user_service, geo_service)


class TestApp(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        user_service = UserService(CSV(USER_DATA_FILE))
        user_service = UserService(CSV(USER_DATA_FILE))
        geo_service = GeoService(CSV(GEO_DATA_FILE))
        search = SearchInShardsService(shards=[SimpleSearchService(CSV(file)) for file in SEARCH_DOCUMENTS_DATA_FILES])
        return get_service(MetaSearchService(search, user_service, geo_service))

    def test_ok(self):
        query = '/search?text={}'.format('politician')
        query += '&user_id={}'.format(25)
        query += '&ip_addr={}'.format('3.103.8.10')
        query += '&limit={}'.format(5)
        response = self.fetch(query)
        assert response.code == 200
        res = json.loads(response.body)['search_results']
        result_keys = [d['key'] for d in res]
        expected_keys = [
            'Dutch crackdown risks hurting mainstream Muslims',
            'Dutch Filmmaker Murder Suspect Faces Terror Charges',
            'Surprise victory for Basescu in Romania',
            'BLAIR PEACE HOPES',
            'France Opens Judicial Inquiry Into Holocaust Doubter'
        ]
        assert result_keys == expected_keys


def test_integration_works(metasearch):
    res = metasearch.search(search_text='politician', user_id=25, ip='3.103.8.10', limit=5)
    result_keys = [d['key'] for d in res]
    expected_keys = [
        'Dutch crackdown risks hurting mainstream Muslims',
        'Dutch Filmmaker Murder Suspect Faces Terror Charges',
        'Surprise victory for Basescu in Romania',
        'BLAIR PEACE HOPES',
        'France Opens Judicial Inquiry Into Holocaust Doubter'
    ]
    assert result_keys == expected_keys


def test_integration_bad_parameters(metasearch):
    metasearch.search(search_text='politician', user_id=None, ip='0.0.0.0', limit=10)
    metasearch.search(search_text='politician', user_id=None, ip=None, limit=1)
    metasearch.search(search_text=None, user_id=None, ip=None, limit=1)
    metasearch.search(search_text='', user_id=None, ip=None, limit=1)
