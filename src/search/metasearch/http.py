from flask import Flask, request
from tornado import httpserver
from tornado import gen
from tornado.ioloop import IOLoop
import tornado.web

from metasearch.service import MetaSearchService


class SearchHandler(tornado.web.RequestHandler):
    def initialize(self, metasearch):
        self.metasearch = metasearch

    def get(self):
        try:
            text = self.get_query_argument('text')
            user_id = int(self.get_query_argument('user_id'))
            ip = self.get_query_argument('ip_addr', None)
            limit = int(self.get_query_argument('limit', 10))
            
            if ip is None:
                ip = self.request.remote_ip
            sr = self.metasearch.search(text, user_id, ip, limit)
            answer = {'search_results': sr}
            self.set_status(200, 'OK')
            self.write(answer)
        except Exception as e:
            self.set_status(500, 'Server error')
            self.write({'error': '{}'.format(e)})


class Server(tornado.web.Application):
    def __init__(self, metasearch):
        handlers = [
            (r"/search", SearchHandler, {'metasearch': metasearch}),
        ]
        tornado.web.Application.__init__(self, handlers)


def get_service(metasearch):
    return Server(metasearch)


def run_server(metasearch):
    app = get_service(metasearch)
    app.listen(8000)
    IOLoop.instance().start()
