from common.data_source import CSV
from settings import USER_DATA_FILE, USER
from user.service import run_user_service


def main():
    run_user_service(CSV(USER_DATA_FILE), USER[1])

if __name__ == '__main__':
    main()
