import json
from tornado import httpserver
from tornado import gen
from tornado.ioloop import IOLoop
import tornado.web

import ipaddress as ip

import pandas as pd

from common.data_source import CSV
import requests


class GeoService:
    _data: pd.DataFrame
    _networks: pd.Series

    def __init__(self, data_source: CSV):
        self._data = data_source.read_data(to_dict=False)
        for col in ('network', 'country_name'):
            assert col in self._data.columns
        self._networks = self._data['network'].apply(ip.ip_network)

    def get_geo_data(self, ip_addr):
        try:
            addr = ip.ip_address(ip_addr)
        except ValueError:
            return None
        addr_in_net = self._networks.apply(lambda x: addr in x)
        addr_in_net = addr_in_net[addr_in_net == True]
        if len(addr_in_net) > 0:
            return {'region': self._data.loc[addr_in_net.head(1).index].iloc[0].country_name}
        return None


class Handler(tornado.web.RequestHandler):
    def initialize(self, geo_service):
        self.geo_service = geo_service

    def get(self):
        try:
            ip_addr = self.get_query_argument('ip_addr', None)
            from sys import stderr as st
            st.write('\n\nip_addr: {} \n\n'.format(ip_addr))
            st.flush()

            if ip_addr is None:
                self.set_status(400, 'BAD REQUEST')
                self.write({'error': 'need ip_addr query parameter'})
                return
            #user_id = int(user_id)
            data = self.geo_service.get_geo_data(ip_addr)
            st.write('\n\nData: {}\n\n'.format(data))
            st.flush()
            self.set_status(200, 'OK')
            self.write({'data': data})
        except Exception as e:
            self.set_status(500, 'Server error')
            self.write({'Error': e})


class GeoServiceApplication(tornado.web.Application):
    def __init__(self, geo_service):
        handlers = [
            (r"/info", Handler, {'geo_service': geo_service}),
        ]
        tornado.web.Application.__init__(self, handlers)


def get_geo_service(data_source):
    geo_service = GeoService(data_source)
    return GeoServiceApplication(geo_service)

def run_geo_service(data_source, port):
    app = get_geo_service(data_source)
    app.listen(port)
    IOLoop.instance().start()

class GeoServiceClient:
    def __init__(self, host, port):
        self.url = 'http://{}:{}/info'.format(host, port)
        from sys import stderr as st
        st.write('\n\nURL: {} \n\n'.format(self.url))
        st.flush()

    def get_geo_data(self, ip_addr):
        from sys import stderr as st
        try:
            url = '{}?ip_addr={}'.format(self.url, ip_addr)
            raw_resp = requests.get(url)
            code = raw_resp.status_code
            st.write('\n\nAnsweer: {} {}\n\n'.format(code, raw_resp.text))
            st.flush()
        except Exception as e:
            st.write('\n\nINTERNAL ERROR: {}\n\n'.format(e))
            st.flush()
            code = 500

        if code != 200:
            return None
        response = json.loads(raw_resp.text)
        return response.get('data')
