import pytest
import pandas as pd
import tornado 
import tornado.testing
import json

from common.data_source import AbstractDataSource
from geo.service import get_geo_service


class SomeDataSource(AbstractDataSource):
    def read_data(self, *args, **kwargs):
        return pd.DataFrame([{'network': '192.168.1.0/24', 'country_name': 'Some Country'}])


class TestApp(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return get_geo_service(SomeDataSource())

    def test_ok(self):
        response = self.fetch('/info?ip_addr=192.168.1.255')
        assert response.code == 200
        assert json.loads(response.body)['data'] == {'region': 'Some Country'}

    def test_empty(self):
        response = self.fetch('/info?ip_addr=192.168.0.1')
        assert response.code == 200
        assert json.loads(response.body)['data'] is None

    def test_empty_2(self):
        response = self.fetch('/info?ip_addr=abc')
        assert response.code == 200
        assert json.loads(response.body)['data'] is None

    def test_bad_request(self):
        response = self.fetch('/info')
        assert response.code == 400
