import numpy as np
import pandas as pd
import pytest
import tornado 
import tornado.testing
import json
from urllib.parse import quote

from common.data_source import AbstractDataSource
from search.service import get_simple_search_service, get_search_in_shards_service, SearchInShardsService, SimpleSearchService


class SomeDataSource(AbstractDataSource):
    def read_data(self):
        return [{'document': 'some document', 'key': 'some_key', 'gender': 'male,female',
                 'age_from': 0, 'age_to': 10, 'region': 'Some Region'},
                {'document': 'another document', 'key': 'another_key', 'gender': 'female',
                 'age_from': 15, 'age_to': 30, 'region': 'Another Region'},
                {'document': 'some another document', 'key': 'some_another_key', 'gender': 'male',
                 'age_from': 20, 'age_to': 25, 'region': 'Some Region'}]


@pytest.fixture
def search_service():
    return SimpleSearchService(SomeDataSource())


class TestApp(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return get_simple_search_service(SomeDataSource())

    def test_ok(self):
        query = '/info?search_text={}'.format(quote('some another'))
        query += '&user_data={}'.format(quote(json.dumps({'gender': 'female', 'age': '21'})))
        query += '&geo_data={}'.format(quote(json.dumps({'region': 'Some Region'})))
        query += '&limit={}'.format(2)
        response = self.fetch(query)
        assert response.code == 200
        res = json.loads(response.body)['data']
        expected = ['some_another_key', 'some_key']
        assert len(expected) == len(res)
        for rec in res:
            assert rec['key'] in expected

    def test_bad_request(self):
        query = '/info'
        response = self.fetch(query)
        assert response.code == 400


def test_build_tokens_count(search_service):
    res = search_service._build_tokens_count('some another')
    assert all(res == pd.Series([1, 1, 2]))


def test_get_geo_mask(search_service):
    res = search_service._get_geo_mask(geo_data={'region': 'Some Region'})
    assert all(res == pd.Series([True, False, True]))


def test_get_gender_mask(search_service):
    res = search_service._get_gender_mask(user_data={'gender': 'female'})
    assert all(res == pd.Series([True, True, False]))


def test_get_age_mask(search_service):
    res = search_service._get_age_mask(user_data={'age': '21'})
    assert all(res == pd.Series([False, True, True]))


class TestApp2(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):

        class SomeDataSource(AbstractDataSource):
            def __init__(self, rows):
                self._rows = rows

            def read_data(self):
                return self._rows

        ds1 = SomeDataSource([{'document': 'some document', 'key': 'some_key', 'gender': 'male,female',
                               'age_from': 0, 'age_to': 10, 'region': 'Some Region'}])
        ds2 = SomeDataSource([{'document': 'another document', 'key': 'another_key', 'gender': 'female',
                               'age_from': 15, 'age_to': 30, 'region': 'Another Region'},
                              {'document': 'some another document', 'key': 'some_another_key', 'gender': 'male',
                               'age_from': 20, 'age_to': 25, 'region': 'Some Region'}])
        shard1 = SimpleSearchService(ds1)
        shard2 = SimpleSearchService(ds2)
           
        return get_search_in_shards_service([shard1, shard2])

    def test_ok(self):
        query = '/info?search_text={}'.format(quote('some another'))
        query += '&user_data={}'.format(quote(json.dumps({'gender': 'female', 'age': '21'})))
        query += '&geo_data={}'.format(quote(json.dumps({'region': 'Some Region'})))
        query += '&limit={}'.format(2)
        response = self.fetch(query)
        assert response.code == 200
        res = json.loads(response.body)['data']
        expected = ['some_another_key', 'some_key']
        assert len(expected) == len(res)
        for rec in res:
            assert rec['key'] in expected

    def test_bad_request(self):
        query = '/info'
        response = self.fetch(query)
        assert response.code == 400
