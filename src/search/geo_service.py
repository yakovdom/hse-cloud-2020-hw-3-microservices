from common.data_source import CSV
from settings import GEO_DATA_FILE, GEO
from geo.service import run_geo_service


def main():
    run_geo_service(CSV(GEO_DATA_FILE), GEO[1])

if __name__ == '__main__':
    main()
