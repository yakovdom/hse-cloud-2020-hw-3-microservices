from common.data_source import CSV
from geo.service import GeoServiceClient
from metasearch.http import Server, run_server
from metasearch.service import MetaSearchService
from search.service import SearchServiceClient, SimpleSearchService
from settings import USER, GEO, SEARCH
from user.service import UserServiceClient


def main():
    user_service = UserServiceClient(*USER)#('user', 8182)
    geo_service = GeoServiceClient(*GEO)#('geo', 8183)
    search = SearchServiceClient(*SEARCH)#('search_in_shards', 8184)
    metasearch = MetaSearchService(search, user_service, geo_service)
    run_server(metasearch=metasearch)


if __name__ == '__main__':
    main()
