import os

BASE_DIR = os.sep.join(os.path.normpath(__file__).split(os.sep)[:-3])
BASE_DATA_DIR = os.path.join(BASE_DIR, 'data')
assert os.path.exists(BASE_DATA_DIR)


def datafile(file):
    return os.path.join(BASE_DATA_DIR, file)


USER_DATA_FILE = datafile('users.csv')
GEO_DATA_FILE = datafile('geo.csv')
SEARCH_DOCUMENTS_DATA_FILES = [datafile(f'news_generated.{i}.csv') for i in range(1, 4)]

USER = ['user', 8182]
GEO = ['geo', 8183]
SEARCH = ['search_in_shards', 8184]
SHARDS = [['shard1', 8185], ['shard2', 8186], ['shard3', 8187]]
