import json
from tornado import httpserver
from tornado import gen
from tornado.ioloop import IOLoop
import tornado.web
from common.data_source import AbstractDataSource
import requests


class UserService:
    key = 'user_id'
    data_keys = ('gender', 'age')

    def __init__(self, data_source: AbstractDataSource):
        self._data = dict()
        data = data_source.read_data()
        for row in data:
            assert row[self.key] not in self._data, f'Key value {self.key}={row[self.key]} is not unique in self._data'
            self._data[row[self.key]] = {k: row[k] for k in self.data_keys}

    def get_user_data(self, user_id):
        return self._data.get(user_id)


class Handler(tornado.web.RequestHandler):
    def initialize(self, user_service):
        self.user_service = user_service

    def get(self):
        try:
            user_id = self.get_query_argument('user_id', None)
            from sys import stderr as st
            st.write('\n\nUser id: {} \n\n'.format(user_id))
            st.flush()

            if user_id is None:
                self.set_status(400, 'BAD REQUEST')
                self.write({'error': 'need user_id query parameter'})
                return
            user_id = int(user_id)
            data = self.user_service.get_user_data(user_id)
            st.write('\n\nData: {} {}\n\n'.format(self.user_service, data))
            st.flush()
            self.set_status(200, 'OK')
            self.write({'data': data})
        except Exception as e:
            self.set_status(500, 'Server error')
            self.write({'Error': e})


class UserServiceApplication(tornado.web.Application):
    def __init__(self, user_service):
        handlers = [
            (r"/info", Handler, {'user_service': user_service}),
        ]
        tornado.web.Application.__init__(self, handlers)


# for tests
def get_user_service(data_source):
    user_service = UserService(data_source)
    return UserServiceApplication(user_service)


def run_user_service(data_source, port):
    app = get_user_service(data_source)
    app.listen(port)
    IOLoop.instance().start()


class UserServiceClient:
    def __init__(self, host, port):
        self.url = 'http://{}:{}/info'.format(host, port)
        print('URL:', self.url)
        from sys import stderr as st
        st.write('\n\nURL: {} \n\n'.format(self.url))
        st.flush()

    def get_user_data(self, user_id):
        url = '{}?user_id={}'.format(self.url, user_id)
        raw_resp = requests.get(url)
        code = raw_resp.status_code
        from sys import stderr as st
        st.write('\n\nAnsweer: {} {}\n\n'.format(code, raw_resp.text))
        st.flush()

        print('ANSWER:', code, raw_resp.text)
        if code != 200:
            return None
        response = json.loads(raw_resp.text)
        return response.get('data')
