import pytest
import tornado 
import tornado.testing
import json

from common.data_source import AbstractDataSource
from user.service import get_user_service


class SomeDataSource(AbstractDataSource):
    def read_data(self):
        return [{'user_id': 1, 'gender': 'male', 'age': 12},
                {'user_id': 2, 'gender': 'female', 'age': 25}]


class TestApp(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return get_user_service(SomeDataSource())

    def test_ok(self):
        response = self.fetch('/info?user_id=1')
        assert response.code == 200
        assert json.loads(response.body)['data'] == {'gender': 'male', 'age': 12}

    def test_ok_empty(self):
        response = self.fetch('/info?user_id=0')
        assert response.code == 200
        assert json.loads(response.body)['data'] is None

    def test_bad_req(self):
        response = self.fetch('/info')
        assert response.code == 400
