from common.data_source import CSV
from settings import SEARCH_DOCUMENTS_DATA_FILES
from search.service import run_simple_search_service
import os
import argparse


def main(args):
    run_simple_search_service(CSV(args.file), args.port)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, required=True, help='port')
    parser.add_argument('--file', type=str, required=True, help='file')
    args = parser.parse_args()
    main(args)
