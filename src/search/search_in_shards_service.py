from common.data_source import CSV
from settings import SHARDS, SEARCH
from search.service import SearchServiceClient, run_search_in_shards_service


def main():
    run_search_in_shards_service([SearchServiceClient(*args) for args in SHARDS], SEARCH[1])

if __name__ == '__main__':
    main()
